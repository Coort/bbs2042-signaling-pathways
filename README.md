# BBS2042: Visualisation and modulation of cell signaling pathways in (patho)physiological conditions

* **Author:** Susan Steinbusch-Coort

* **Last updated** 2023-02-27

----

### Introduction
In this practical training pathway visualisation will be applied to monitor cellular signalling pathways. The modulation of single signal transduction routes based on measurements at protein level will be visualized. In addition, integration of different cell signalling pathways will be visualized to gain a better understanding of the complexity of the cell signalling network. A commonly used and at Maastricht University developed pathway visualization and analysis tool, PathVisio [1], will be used to show proteins at pathway level.

----

### Goals
Learn how to visualize (phospho)proteomic data in pathways using PathVisio.
Learn how insulin can influence phosphorylation of the proteins in the insulin signaling cascade.
Investigate other cellular pathways in which insulin influences the protein phosphorylation.
Visualize how inhibition of Akt affects the insulin-induced phosphorylation.

----

### Organization
Dr. Susan Steinbusch-Coort
Assistant Professor
Department of Bioinformatics (BiGCaT), NUTRIM, FHML, Maastricht University       
Contact: susan.coort@maastrichtuniversity.nl 

----
### Instructors
* Dr. Friederike Ehrhart
Assistant Professor
Department of Bioinformatics (BiGCaT), NUTRIM, FHML, Maastricht University 
* Javier Millan A Costa, MSc.
PhD Student
Department of Bioinformatics (BiGCaT), NUTRIM, FHML, Maastricht University 

----

### Schedule and setup
The practical training will start with a short introduction on pathway visualization.  
The practical session will take place on-site in the computer rooms. 

----

### Signing off
* Students need to sign off their attendance sheet. 
----

#### Introduction Lecture: Pathway analysis using various omics data
* During this one-hour lecture an introduction to pathway analysis will be given. First, all the ingredients for pathway analysis, i) biological pathways (available at [WikiPathways](https://www.wikipathways.org/) [2,3]), ii) identifier mapping and iii) omics data, will be explained. Second, the in-house developed and open-source pathway visualization and analysis tool, [PathVisio](https://pathvisio.github.io/) [1] will be introduced. Finally, examples of pathway visualization using various omics data, like transcriptomics and proteomics will be demonstrated.


#### Practical sessions: Pathway analysis in PathVisio
* Tuesday/Wednesday/Thursday, 15-17 March 2022
* Pathway analysis
* You will analyse a phosphoproteomics dataset using pathway analysis in PathVisio. The publicly available dataset used in this session is published by Zhang et al [4] and compares primary rat hepatocytes treated with insulin to untreated hepatocytes. In addition, three protein kinase inhibitors were used to investigate whether inhibiting proteins in the signaling cascade influence the insulin-induced phosphorylation. The different inhibitors used are seen in the figure below. In the current practical you will only investigate the effect of the Akt inhibitor.

![alt text](img/Insulin-interfering.gif "Downstream effect of insulin")

The figure describes the key downstream kinases of insulin. Upon binding of insulin to its membrane receptor a series of phosphorylation actions occur.Moreover three protein kinase inhibitors, i.e., MK2206, Rapamycin and LYS6K1 inhibiting the action of Akt, mTOR and S6K, respectively, are shown

----

### Literature
1. Kutmon, M, et al. "PathVisio 3: an extendable pathway analysis toolbox." PLoS Comput Biol 11.2 (2015): e1004085. [doi: 10.1371/journal.pcbi.1004085](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004085).
2. Kutmon, M, et al. "WikiPathways: capturing the full diversity of pathway knowledge." Nucleic acids research (2015): gkv1024. [doi: 10.1093/nar/gkv1024](https://academic.oup.com/nar/article/44/D1/D488/2502580).
3. Slenter, DN, et al. "WikiPathways: a multifaceted pathway database bridging metabolomics to other omics research." Nucleic Acids Res (2018)  Jan 4;46(D1):D661-D667. [doi: 10.1093/nar/gkx1064](https://academic.oup.com/nar/article/46/D1/D661/4612963).
4. Zhang Y, et al. "Global phophoproteomics analysis of insulin/Akt/mTORC1/S6K signaling in rat hepatocytes." J  Proteome Res.  16.8 (2017): 2825-2835. [doi: 10.1021/acs.jproteome.7b00140](https://pubs.acs.org/doi/10.1021/acs.jproteome.7b00140)
 

