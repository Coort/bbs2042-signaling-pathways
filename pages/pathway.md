## Preparation!
* Check the software installation instructions 
* Download the BBS2042-Practical3.zip file from the student portal (on the UM remote connection, save the zip file in C: > Users > Public > Public Downloads). Unzip the zip file (Right-click > 7-zip > Extract Here). This folder contains all data and files required for the practical. 


## Step 1: Start PathVisio and open the rat insulin signaling pathway from WikiPathways
* Open the PathVisio software 
* Then load the gene identifier mapping database (Data -> Select Gene Database -> Load Rn_Derby_Ensembl_89.bridge)
* Open the insulin signaling pathway. Go to File -> Open -> Go to the Rat_pathway-collection folder -> Select Rn_Insulin_induced_Pi3K_Akt_and_MAPK_WP4229_96334.gpml -> Open



### Questions

1a) How is the Forkhead box protein O1 (Foxo1) annotated in the pathway? Go to the backpage panel to see the annotation.
<br>
1b) To which other databases does the gene link?

----

## Step 2: Statistically analyzed dataset 
In the zip file you can find multiple datasets. We will start with the dataset_insulin-screen.txt. You can have a look at the data file in Excel. Here you will find the following columns:


![alt text](img/Dataset-sorted.png "Example dataset")


#### Explanation of the columns:
Gene Symbol: this column contains the gene names.
* Ensembl: this column contains the Ensembl identifiers for the genes encoding the proteins
* log2 Ratio (H/L): the fold change is a metric for comparing phosphorylation of a peptide. Log transformed data is easier to handle statistically. Here we compared insulin treated rat hepatocytes  to untreated hepatocytes. H = heavy (insulin) and L = light (control).

----

## Step 3: Import the data 
Now back in PathVisio: In the menu bar, click Data -> Import Expression Data. Use the Browse buttons to locate the following files:
* Input file: The experiment data file (dataset_insulin-screen.txt).
* Output file: Will be filled in automatically after selecting the input file, you don't need to change this.
* Gene database: Use the gene identifier mapping database in the zip file (Rn_Derby_Ensembl_89.bridge). If you loaded the gene database in the preparation, you don't need to change the file here.

![alt text](img/Importing_phosphodata_1.png "Importing PhopshoData 1")

Now follow the wizard to import the dataset (check the settings and then click on "Next").

![alt text](img/ImportData1.PNG "Importing PhopshoData 2")

Select the columns that contain the identifiers (1) and identifier type (2). The database contains Ensembl identifiers, so make sure "Ensembl" is selected in the system code drop-down box. The identifier column should be highlighted in green (Ensembl).

![alt text](img/ImportData2.PNG "Importing PhopshoData 3")

The data will now be imported into an expression dataset that is saved as a .pgex file on your harddisk (in the same directory as the dataset). Any exceptions will be reported to the file .pgex.ex (also in the same directory as the dataset). For example, a common exception occurs when an identifier is not found in the identifier mapping database and is therefore not included in the dataset.
* Please note that if you have an equal amount of "rows succesfully imported" and "identifiers not recognized" -> something went wrorng with importing the data !!

![alt text](img/ImportData3.PNG "Importing PhopshoData 4")

To test if the dataset has been created correctly, click on a gene/protein in the pathway and the imported data will appear in the data panel (see tabs on the right side). If you don't see data for the proteins, the import was not successful! Please redo the steps or ask an instructor for help.

![alt text](img/ImportData4.PNG "Importing PhopshoData 5")

Well done! You successfully imported the phosphorylation dataset in PathVisio. Next we need to create a visualization that intuitively shows if proteins are more or less phosphorylated after treatment.

----

## Step 4a: Create a visualization that resembles figure 5

In PathVisio you can make different visualizations to color the elements in a pathway based on an expression dataset. We want to visualize to what extent and in which direction the phoshorylation of a protein has changed in the data set. Please note that per protein multiple phosphorylation sites can be measured. If we want to visualize the changes in phosphorylation, we have to create a new visualization.
* Go to "Data -> Visualization options". The visualization options dialog will open.
* Create a new visualization by clicking the button in the top-right corner and select "New"

![alt text](img/VizData1.png "Visualization_1")

* Specify a name for the visualization (e.g. "Advanced visualization")
* Check the box before "Expression as color" and the box before "Text label".
* In the options panel below "Expression as color", select the Log2 FC Ratio (H/L) data column
* Create a new color set by clicking the button to the right of the color set dropdown box and select “New” to create a new color set.

![alt text](img/VizData2.png "Visualization 2")


* The color set dialog will now open. 

![alt text](img/VizData3.png "Visualization 3")

* At the bottom of the dialog, click “Add Rule”.
* In the text field beside “Rule Logic” use the following rule expression: ” [Log2 Ratio (H/L)] > 1 ″.
* At the bottom click on “Color” and select an orange color.
* Now make two additonal rules:
* [Log2 Ratio (H/L)] <= 1 AND [Log2 Ratio (H/L)] >= -1 and click on"Color" and select white
* [Log2 Ratio (H/L)] < -1  and click  on "Color" and select a dark blue color.

![alt text](img/Importing_phosphodata_9.png "Visualization 4")

* Click “Ok” to apply the rule and close the color set dialog
* Click “Ok” and have a look at the changes in the pathway

![alt text](img/Visualization_rules3.png "Visualization 5")

### Questions

2a) How are the various measured phosphorylation sites visualized on the protein in the pathway? _(Hint: click on the Data tab on the right and see what data is visualized on the data node - keep in mind that each row in the dataset represents one phosphorylation site)_
<br>
2b) What do the colors of the protein boxes in the pathway represent?

----

## Step 4b: Create a visualization using a gradient
Go to "Data -> Visualization options". The visualization options dialog will open.
Create a new color set by clicking the button to the right of the color set dropdown box and select "New" to create a new color set.
The color set dialog will now open. Click the checkbox before "Gradient" to activate the color gradient.
Select the gradient that runs from blue to white to red and specify the limits in the boxes below the gradient, e.g. -1, 0, 1.

![alt text](img/Visualization_2.png  "Visualization gradient 1")

* Click "Ok" to apply the rule and close the color set dialog
* Click "Ok" and have a look at the changes in the pathway.

![alt text](img/Visualization_gradient.png  "Visualization gradient 2")

* Now have a closer look at the visualization of the phosphorylation per protein.
* Select the FoxK1 protein in the pathways (note that it is a downstream target of Akt).
* Open the "Data" side panel at the right hand side.

### Question 

3) What do the colored boxes represent now?  


----

## Step 5: Save the pathway showing the phosphoproteomics data as an image
Save the insulin pathway including the visualization of the data as .png. Go to File -> Export -> Select PNG as file type and Export.
Please note that if you save the image under "Public" it will be lost, so save it at your own I-drive.

![alt text](img/Export.png  "Export")

----

## Step 6: Investigation of the changes in other cell signaling pathways upon stimulation by Insulin
The rat_pathway-collection has, in addition to the insulin signaling pathway, also other processes. Investigate those as well and see how the phosphorylation is affected by insulin in rat hepatocytes.
Go to File-> Open and select one of the other pathways. Note that the insulin signaling pathway from WikiPathways is among the selected pathways.

### Question 

4) This allows you to see if insulin treatment affects the phosphorylation levels of proteins in other pathways as well. Select two pathways and explain what you see in detail.

----

## Step 7: Investigation of the changes in the signaling pathways when inhibiting the Akt protein 
Now repeat Steps 1 to 5 using the phosphoproteomics data in which the Akt protein is inhibited. The dataset to use is the "dataset_Akti-screen.txt" in the downloaded zip file and answer the following questions by comparing it to the effect of insulin alone. Be aware that the log2 Ratio (H/L) now compares Akti- and insulin-treated rat hepatocytes with insulin-treated hepatocytes. 

### Questions 

5a) How does the inhibition of Akt influence the phosphorylation of the proteins induced by insulin? 
<br>
5b) What is different from the phosphorylation of the proteins when only insulin is given? Note that you saved the image in step 5, this helps you to compare the two pathways. 
<br>
5c) What is happening in the other pathways listed in "rat_pathway-collection" if rat hepatocytes are treated with an Akt inhibitor?

----

## Optional Step 8: Create a new pathway in PathVisio 
If you want to learn how to create a pathway in PathVisio, we suggest to have a look at http://academy.wikipathways.org/. You can download the correct identifier gene mapping database for your species of interest from the [Bridgedb website](https://bridgedb.github.io/) and the set of biological pathways from [WikiPathways](https://www.wikipathways.org/index.php/Download_Pathways). Please make sure you select the correct species when downloading the mapping database and collection of pathways.

----

### Literature

1. Zhang Y, et al. "Global phophoproteomics analysis of insulin/Akt/mTORC1/S6K signaling in rat hepatocytes." J  Proteome Res.  16.8 (2017): 2825-2835. [doi: 10.1021/acs.jproteome.7b00140](https://pubs.acs.org/doi/10.1021/acs.jproteome.7b00140)
1. Kutmon, M, et al. "PathVisio 3: an extendable pathway analysis toolbox." PLoS Comput Biol 11.2 (2015): e1004085. [doi: 10.1371/journal.pcbi.1004085](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004085).
1. Kutmon, M, et al. "WikiPathways: capturing the full diversity of pathway knowledge." Nucleic acids research (2015): gkv1024. [doi: 10.1093/nar/gkv1024](https://academic.oup.com/nar/article/44/D1/D488/2502580).
1. Slenter, DN, et al. "WikiPathways: a multifaceted pathway database bridging metabolomics to other omics research." Nucleic Acids Res (2018)  Jan 4;46(D1):D661-D667. [doi: 10.1093/nar/gkx1064](https://academic.oup.com/nar/article/46/D1/D661/4612963).
