### Outline
We provide datasets containing phosphoproteomics data. Quality control, normalization and statistical analysis were already performed.
The goal is to explore the phosphorylated proteins in the cell signaling pathways after insulin treatment in rat hepatocytes.

----

### Dataset
In the study by Zhang Y et al [1] they measured the global phosphoproteome in rat hepatocytes. A ReDi-based proteomics analysis was performed in order to characterize the insulin-regulated phosphoproteome in these hepatocytes. In the first experiment, they compared starved primary hepatocytes that were subsequently treated with or without 100nM insulin for 30 min. This allows us to identify the proteins whose phosphorylation is stimulated by insulin. In the second experiment, they pre-treated primary hepatocytes with or without MK2206 (a specific, potent inhibitor of Akt) and then stimulated both groups with insulin to identify insulin effector proteins whose phosphorylation is sensitive to Akt inhibition

![alt text](img/Figure5-paper.gif "Phosphorylation events")

In the paper the analyzed phosphoproteomics datasets are available as supplementary material. In our practical we will use the analyzed data. Figure 5 in the paper of Zhang Y et al. [1] shows the insulin induced PI3K -Akt and MAPK network in hepatocytes. The pathway figure represents summarized literature-curated knowledge of the PI3K-Akt, MAPK and mTORC1 signaling networks. Phosphorylation sites are shown with colored circles; orange, increased by insulin; blue, decreased by insulin; gray, no chang. 
Today you will use the program PathVisio to visualize the phosphorylation of proteins in the insulin signaling pathway but also in other selected signaling processes. Slides of the introduction can be found [here](https://surfdrive.surf.nl/files/index.php/s/mtYLX9VMdsDNwUT).

----

### Literature
1. Zhang Y, et al. "Global phophoproteomics analysis of insulin/Akt/mTORC1/S6K signaling in rat hepatocytes." J  Proteome Res.  16.8 (2017): 2825-2835. [doi: 10.1021/acs.jproteome.7b00140](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004085)