### Preparation

#### Downloading data

* You can download the zip file required for the session from [here](https://surfdrive.surf.nl/files/index.php/s/ExQnY3C7ceZJ5he).
* When being at the Computer rooms at Maasticht University, download the file at a specific location: ***Your ID number -> Local Disk (C:) -> Users -> Public*** . You need to save it here because otherwise you could run into problems due to storage size limitations.
* Note that you need **to extract **the files before you can use it

##### The zip file contains the following files and folders:
* **Rat_pathway-collection**: A set of rat pathways downloaded from WikiPathways that will be used in the pathway visualization (it also includes the insulin signaling pathway)
* **dataset_insulin-screen.tx**t: statistically analyzed phosphoproteomic dataset
* **dataset_Akti-screen.txt**: statistically analyzed phosphoproteomic dataset
* **Rn_Derby_Ensembl_89.bridge**: human gene identifier mapping database

----

#### Using PathVisio in the computer rooms. 
The program PathVisio is installed in the computer rooms at Maastricht University (Randwyck)
<br>
To open the program go to Start -> PathVisio

----

#### Using PathVisio on your own laptop/PC

##### ***Installation***
Current version: [PathVisio 3.3.0](https://github.com/PathVisio/pathvisio/releases/download/v3.3.0/pathvisio_bin-3.3.0.zip)
Download the file and unzip it.
<br>
**On Windows:** start PathVisio by double-clicking the pathvisio.bat file
<br>
**On MacOSX / Linux:** start PathVisio by running pathvisio.sh or double-click the pathvisio.jar file

##### ***Requirements***
PathVisio requires the installation of Java 8
<br>
**Java** can be downloaded [here](https://www.java.com/nl/download/)